﻿namespace Mango.Service.AuthAPI.Models
{
    public class JwtOptions
    {
        public string Issuer { get; set; } = string.Empty;
        public string Audience { get; set; } = string.Empty;
        public string Secret { get;set; } = "kGZza8tiTIK2IChchwLbLYRzkDNK7AWbGeqSRihXXA88FUmuyjnmxvkPtJbk4pURdF9htSPhX3MiLw9CyZRZhfdIvaZE1XltTodO5OPmZgykm5tGTz2xnYTK9WdwN2b7";
    }
}
