﻿using Newtonsoft.Json;

namespace Mango.Services.OrderAPI.Models.Dto
{
    public class CartDto
    {
        public CartHeaderDto CartHeader { get; set; }
        [JsonProperty("cartDetails")]
        public IEnumerable<CartDetailsDto>? CartDetails { get; set; }
    }
}
